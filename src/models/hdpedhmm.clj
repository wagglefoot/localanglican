(ns models.hdpedhmm
  "This model needs a new name.
   This namespace just stores the whole thing")


(with-primitive-procedures
  [shape
   mmul
   model-params
   DP-ind]
  (defquery hdp-edhmm
    "HDP-EDHMM"
    [observations]
    (let
      [ ;; ==== Initialize Processes for State and Durations Transitions ====

        alpha-trans-0 (sample (model-params :alpha-trans-0-prior))
        _ (store "state-transition-base-process" (DP alpha-trans-0
                                                     (model-params :trans-dist)))

        alpha-dur-0 (sample (model-params :alpha-dur-0-prior))
        _ (store "duration-base-process" (DP alpha-dur-0
                                             (model-params :dur-dist)))

        ;; ==== Specify State and Duration Transitions ====

        alpha-dur (sample (model-params :alpha-dur-prior))
        transition-duration
        (fn [S D]
          (if (< 1 D)
            (- D 1)
            (let [ duration-base-process (retrieve "duration-base-process")
                   duration-group-process (or (retrieve ["duration-group-process" S])
                                              (DP-ind alpha-dur (produce duration-base-process)))
                   newpair  (sample (produce  duration-group-process))
                   newduration (first newpair)
                   newind (second newpair)
                   _ (if newind (store "duration-base-process" (absorb duration-base-process newduration)) 0)
                   _ (store ["duration-group-process" S] (absorb duration-group-process newduration))]
              newduration)))

        alpha-trans (sample (model-params :alpha-dur-prior))
        transition-state
        (fn [S D]
          (if (< 1 D)
            S
            (let [ transition-base-process (retrieve "state-transition-base-process")
                   transition-group-process (or (retrieve ["state-transition-process" S])
                                                (DP-ind alpha-trans (produce transition-base-process)))
                   newpair (loop [a [S false]]
                             (if (= (first a) S)
                               (recur (sample (produce  transition-group-process)))
                               a))
                   newstate (first newpair)
                   newind (second newpair)
                   _ (if newind (store "state-transition-base-process" (absorb transition-base-process newstate)) 0)
                   _ (store ["state-group-transition-process" S] (absorb transition-group-process newstate))]
              newstate)))

        markov-step
        (fn [prev-item]
          (let [s (transition-state (first prev-item) (second prev-item))]
            [s (transition-duration s (second prev-item))]))

        ;; ==== Initialize Observation Process ====

        emperical-observation-mean (mean observations 0)
        rescaled-emperical-observation-cov (mmul
                                             (model-params :cov-mat-scale)
                                             (covariance observations 0))
        dof (+ 2 (second (shape observations)))
        inverse-cov-scale (sample (model-params :inverse-cov-scale))

        obs-base-proc  (mvn-niw
                         emperical-observation-mean
                         inverse-cov-scale      	;; lambda
                         dof        				;; nu
                         rescaled-emperical-observation-cov)]

      ;; ==== Condition on observations ====

      (reduce
        (fn [states-and-durations obs]
          (let [s_d (markov-step (peek states-and-durations))
                s (first s_d)
                proc (or (retrieve ["observation-process" s])
                         obs-base-proc)]

            (observe (produce proc) obs)
            (store ["observation-process" s] (absorb proc obs))
            (conj states-and-durations s_d)))
        (let [state-transition-base-process (retrieve "state-transition-base-process")
              duration-base-process (retrieve "duration-base-process")
              s (sample (produce state-transition-base-process))  ;; Samples base distribution of HDP for states
              d (sample (produce duration-base-process))
              _ (store "state-transition-base-process" (absorb state-transition-base-process s) )
              _ (store "duration-base-process" (absorb duration-base-process d))]
          [[s d]])
        observations))))
