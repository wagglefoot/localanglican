(ns models.statefulhdphmm
  "Stateful hierarhical Dirchlet process hidden Markov model.")

(with-primitive-procedures
  [shape
   mmul
   model-params
   mixture
   DP-ind]
  (defquery stateful-hdp-hmm
    "Stateful HDP-HMM"
    [observations]
    (let
      [ ;; ==== Initialize Processes for State Transitions ====

        alpha-trans-0 (sample (model-params :alpha-trans-0-prior))
        _ (store "state-transition-base-process" (DP alpha-trans-0
                                                     (model-params :trans-dist)))

        ;; ==== Specify State Transitions ====

        get-alpha (mem (fn [S] (sample (model-params :alpha-trans-prior))))
        get-kappa (mem (fn [S] (sample (model-params :kappa-prior))))
        transition-state
        (fn [S]
          (let [
                 transition-base-process (retrieve "state-transition-base-process")
                 transition-group-process (or (retrieve ["state-transition-process" S])
                                              (DP-ind (+ (get-alpha S)
                                                         (get-kappa S))
                                                      (mixture (produce transition-base-process)
                                                               (categorical {S 1})
                                                               (/ (get-alpha S)
                                                                  (+ (get-alpha S) (get-kappa S))))))
                 newpair (sample (produce transition-group-process))
                 newstate (first newpair)
                 newind (second newpair)
                 _ (if newind (store "state-transition-base-process" (absorb transition-base-process newstate)) 0)
                 _ (store ["state-transition-process" S] (absorb transition-group-process newstate))]

            newstate))

        ;; ==== Initialize Observation Process ====

        emperical-observation-mean (mean observations 0)
        rescaled-emperical-observation-cov (mmul (model-params :cov-mat-scale)
                                                 (covariance observations 0))
        dof (+ 2 (second (shape observations)))
        inverse-cov-scale (sample (model-params :inverse-cov-scale))

        obs-base-proc  (mvn-niw
                         emperical-observation-mean
                         inverse-cov-scale
                         dof
                         rescaled-emperical-observation-cov)]

      ;; ==== Condition on observations ====

      (reduce
        (fn [states obs]
          (let [s (transition-state (first (peek states)))
                proc (or (retrieve ["observation-process" s])
                         obs-base-proc)]
            (observe (produce proc) obs)
            (store ["observation-process" s] (absorb proc obs))
            (conj states [s])))
        (let [ state-transition-base-process (retrieve "state-transition-base-process")
               s (sample (produce state-transition-base-process))
               _ (store "state-transition-base-process" (absorb state-transition-base-process s))]  ;; Samples base distribution of HDP for states
          [[s]])
        observations))))
