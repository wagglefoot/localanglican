(ns anglib.specialDistsAndProcs
  "This namespace contains special functions
  that do not exist inside of anglican."
  (:use [anglican core runtime emit]))


;; Special variations with indicator variables included
(defdist categorical-dp-ind
  "categorical distribution extended
  by a random sample, for use with DP"
  [counts H alpha] [dist (categorical
                           (vec (conj counts [::new alpha])))]
  (sample* [this]
           (let [s (sample* dist)]
             ;; When a `new' value is drawn, sample the actual
             ;; value from the base measure.
             (if (= s ::new) [(sample* H) true] [s false])))
  (observe* [this value]
            (log-sum-exp
              ;; The value is one of absorbed values.
              (observe* dist value)
              ;; The value is drawn from the base distribution.
              (+ (observe* dist ::new) (observe* H (first value))))))


(defproc DP-ind
  "Dirichlet process"
  [alpha H] [counts {}]
  (produce [this] (categorical-dp-ind counts H alpha))
  (absorb [this sample]
          (DP-ind alpha H (update-in counts [sample] (fnil inc 0)))))


;; Two part mixture
(defdist mixture [dist1 dist2 mixingprop]
  (sample* [this] (if (sample* (flip mixingprop)) (sample* dist1) (sample* dist2)))
  (observe* [this value]
            (Math/log (+ (* mixingprop
                            (Math/exp (observe* dist1 value)))
                         (* (- 1 mixingprop ) (Math/exp (observe* dist2 value)))))))



;; Multi component mixture
(defdist multi-mixture [list-of-distributions list-of-probabilities]
  (sample* [this] (sample*
                    (sample*
                      (categorical
                        (into {}
                              (map vector list-of-distributions list-of-probabilities))))))
  (observe* [this value]
            (Math/log
              (sum
                (map (fn [x y] (Math/exp
                                 (* (observe* x value ) y)))
                     list-of-distributions list-of-probabilities)))))