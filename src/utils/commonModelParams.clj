(ns utils.commonModelsParams
  "Model parameters to be used across BNP models"
  (:use [anglican runtime]))

(def model-params
  {
    ;; Common to all models
    :base-approx-count 1000
    :alpha-trans-0-prior (gamma 5 1)
    :alpha-trans-prior (gamma 5 1)
    :trans-dist (normal 0 1) ;; It makes more sense to use poisson here, but then it isn't an HDPHMM anymore, like (poisson 1)

    ;; emission prior
    :cov-mat-scale 0.75						;; Murphy p. 133
    :inverse-cov-scale (gamma 0.25 25)		;; Murphy p. 133

    ;; For both sticky HDP-HMM, stateful HDP-HMM
    :kappa-prior (gamma 2.5 1)

    ;; HDP-EDHMM
    :alpha-dur-0-prior (gamma 5 1)
    :alpha-dur-prior (gamma 5 1)
    :dur-dist (uniform-discrete 25 125)

    })
