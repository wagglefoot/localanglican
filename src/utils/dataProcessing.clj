(ns utils.dataProcessing
  "Data preprocessing functions and the like."
  (:use [clojure.data.csv :only [read-csv write-csv]]
        [clojure.java.io :only [writer]]
        [clojure.core.matrix :only [matrix]]))

(defn write2csv [filename data]
  (with-open [f (writer filename)]
    (write-csv f data)))

(defn load-data-from-file [file]
  (matrix
   (let [data (read-csv (slurp file))]
     (for [line data]
       (mapv #(Float/parseFloat %) line)))))

